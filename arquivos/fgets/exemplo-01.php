<?php

$filename = "usuarios.csv";

if(file_exists($filename)){
    $file = fopen($filename, "r"); // r de read
    $headers = explode(",",fgets($file));
    $data = array();

    while($row = fgets($file)){
        $rowData = explode(",", $row);
        $linha = array();

        for($i = 0; $i < count($headers); $i++){
            $linha[$headers[$i]] = $rowData[$i];  //$headers[$i]: pegando as posições do headers         
        }

        array_push($data, $linha);
    }

    fclose($file);
    json_encode($data); 
}

?>