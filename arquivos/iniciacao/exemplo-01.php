<?php

header('Content-Type: text/html; charset=utf-8');

$name = "images";

if(!is_dir($name)){
    mkdir($name, 0777, true); //se o diretório não existir, crie-o
    echo "Diretório " . $name . " criado com sucesso";
} else{
    rmdir($name); //se o diretório existir, remova-o
    echo "$name removido!";
}

?>