<?php
/*
$file = fopen("log.txt", "w+"); //w = write, não sobrescreve o arquivo; + pois se o arquivo não existir, ele criará
fwrite($file, date("d/m/Y H:i:s") . "\r\n");
fclose($file);
echo "Arquivo criado";
*/

$file = fopen("log.txt", "a+"); //o "a" move o ponteiro para o final do arquivo e o sobrescreve; + pois se o arquivo não existir, ele criará
fwrite($file, date("d/m/Y H:i:s") . "\r\n"); //quebra de linha
fclose($file);
echo "Arquivo modificado";
?>