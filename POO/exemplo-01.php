<?php

header('Content-Type: text/html; charset=utf-8');

class Pessoa {
    public $nome;

    public function falar(){
        return "O meu nome é: " . $this->nome;
    }

}

$glaucio = new Pessoa();
$glaucio->nome = "Gláucio Daniel";
echo $glaucio->falar();

?>