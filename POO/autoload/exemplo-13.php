<?php

header('Content-Type: text/html; charset=utf-8');

function incluirClasses($nomeClasse){
    if(file_exists("$nomeClasse.php")){
        require_once("$nomeClasse.php");
    }
}

spl_autoload_register("incluirClasses"); //standard php library

spl_autoload_register(function($nomeClasse){

    if(file_exists("abstratas" . DIRECTORY_SEPARATOR . "$nomeClasse.php")){
        require_once("abstratas" . DIRECTORY_SEPARATOR . "$nomeClasse.php");
    }

});

$carro = new DelRey();
echo $carro->acelerar(80);

?>