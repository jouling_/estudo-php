<?php

header('Content-Type: text/html; charset=utf-8');

interface Veiculo{
    public function acelerar($velocidade);
    public function frenar($velocidade);
    public function trocarMarcha($marcha);
}

abstract class Automovel implements Veiculo{
    public function acelerar($v){
        return "O veículo acelerou até: " . $v . " km/h <br>";
    }

    public function frenar($v){
        return "O veículo frenou até: " . $v. " km/h <br>";
    }

    public function trocarMarcha($m){
        return "A marcha é: " . $m. "<br>";
    }
}

class DelRey extends Automovel{
    public function empurrar(){
        
    }
}

$carro = new DelRey();
echo $carro->acelerar(40);

?>