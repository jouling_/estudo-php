<?php

abstract class Animal{
    public function falar(){
        return "Som";
    }

    public function mover(){
        return "Anda";
    }
}

class Cachorro extends Animal{
    public function falar(){
        return "Late";
    }
}

class Gato extends Animal{
    public function falar(){
        return "Mia";
    }
}

class Passaro extends Animal{
    public function falar(){
        return "Canta";
    }

    public function mover(){
        return "Voa e " . strtolower(parent::mover());
    }
}

$pluto = new Cachorro();
echo $pluto->falar() . "<br>" . $pluto->mover() . "<br>";


$garfield = new Gato();
echo $garfield->falar() . "<br>" . $garfield->mover() . "<br>";

$ave = new Passaro();
echo $ave->falar() . "<br>" . $ave->mover() . "<br>";
?>