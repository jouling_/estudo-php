<?php

header('Content-Type: text/html; charset=utf-8');

class Endereco{
    private $logradouro;
    private $numero;
    private $cidade;

    public function getLogradouro(){
        return $this->logradouro;
    }

    public function setLogradouro($logradouro){
        $this->logradouro = $logradouro;
    }

    public function getNumero(){
        return $this->numero;
    }

    public function setNumero($numero){
        $this->numero = $numero;
    }

    public function getCidade(){
        return $this->cidade;
    }

    public function setCidade($cidade){
        $this->cidade = $cidade;
    }

    public function __construct($a, $b, $c){
        $this->logradouro = $a;
        $this->numero = $b;
        $this->cidade = $c;
    }

    public function __destruct(){
        var_dump("Destruct chamado");
    }

    public function __toString(){
        return $this->logradouro . ", " . $this->numero. " - ". $this->cidade;
    }
}

$meuEndereco = new Endereco("Rua Dom Luis de Bragança", 165, "São Paulo");
var_dump($meuEndereco);
//object(Endereco)#1 (3) { ["logradouro":"Endereco":private]=> string(25) "Rua Dom Luis de Bragança" ["numero":"Endereco":private]=> int(165)
// ["cidade":"Endereco":private]=> string(10) "São Paulo" } 
echo "<br>";
echo $meuEndereco; //usa o toString
echo "<br>";
unset($meuEndereco); //usa o destruct = string(16) "Destruct chamado" 
echo "<br>";
var_dump($meuEndereco); //dá null pois foi destruído
?>