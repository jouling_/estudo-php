<?php

class Pessoa{
    public $nome = "Juliana Hipolito"; //todo mundo vê
    protected $idade = 17; //só é visto na mesma classe e na classe extendida
    private $senha = "123456"; //só é visto na mesma classe

    public function verDados(){
        return $this->nome. "<br>" . $this->idade . "<br>" . $this->senha . "<br>";
    }

}

$objeto = new Pessoa();
/*
echo $objeto->nome . "<br>"; //aparece por ser público
echo $objeto->idade . "<br>"; //n aparece por ser protected
echo $objeto->senha . "<br>"; //n aparece por ser private
*/
echo $objeto->verDados();
?>