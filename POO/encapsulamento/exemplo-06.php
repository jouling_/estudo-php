<?php

class Pessoa{
    public $nome = "Juliana Hipolito"; 
    protected $idade = 17; 
    private $senha = "123456";

    public function verDados(){
        return $this->nome. "<br>" . $this->idade . "<br>" . $this->senha . "<br>";
    }
}

class Programador extends Pessoa{

    public function verDados(){
        echo get_class($this). "<br>"; //retorna Programador
        return $this->nome. "<br>" . $this->idade . "<br>" . $this->senha ."<br>"; 
    }
    //senha não é acessado pois é privado
    

    public function getIdade(){
        return $this->idade;
    }
}

$objeto = new Programador();

echo $objeto->verDados(); //quando chamado sem Programador implementar o método, aparece a senha pela herança
//quando chamado e Programador implementa o método, não aparece a senha


?>