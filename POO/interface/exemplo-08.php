<?php

header('Content-Type: text/html; charset=utf-8');

interface Veiculo{
    public function acelerar($velocidade);
    public function frenar($velocidade);
    public function trocarMarcha($marcha);
}

class Civic implements Veiculo{
    public function acelerar($v){
        return "O veículo acelerou até: " . $v . " km/h <br>";
    }

    public function frenar($v){
        return "O veículo frenou até: " . $v. " km/h <br>";
    }

    public function trocarMarcha($m){
        return "A marcha é: " . $m. "<br>";
    }

}

$carro = new Civic();
echo $carro->trocarMarcha(4);
echo $carro->frenar(60);
echo $carro->acelerar(100);

?>