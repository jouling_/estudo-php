<?php

class Documento{
    private $numero;
    
    public function getNumero(){
        return $this->numero;
    }

    public function setNumero($numero){
        $this->numero = $numero;
    }
}

class Cpf extends Documento{
    public function validar():bool{
        $numeroCpf = $this->getNumero();
        //validação do cpf 
        return true;
    }
}

$doc = new Cpf();
$doc->setNumero("49732723882");
var_dump($doc->validar()); //bool(true)
echo "<br>";
echo $doc->getNumero();//49732723882

?>