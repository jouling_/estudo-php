<?php

header('Content-Type: text/html; charset=utf-8');

class Carro{
    private $modelo;
    private $motor;
    private $ano;

    public function getModelo(){
        return $this->modelo;
    }

    public function setModelo($modelo){
        $this->modelo = $modelo;
    }

    public function getMotor(){
        return $this->motor;
    }

    public function setMotor($motor){
        $this->motor = $motor;
    }

    public function getAno():int{
        return $this->ano;
    }

    public function setAno($ano){
        $this->ano = $ano;
    }

    public function exibir(){
        return array(
            "modelo"=>$this->getModelo(),
            "motor"=>$this->getMotor(),
            "ano"=>$this->getAno()
         );
    }
} //fechando a classe Carro

$gol = new Carro();
$gol->setModelo("Gol GT");
$gol->setMotor("Não conheço");
$gol->setAno(1990);

var_dump($gol->exibir());

echo "<br>";

echo $gol->getModelo();
echo "<br>";
echo $gol->getMotor();
echo "<br>";
echo $gol->getAno();

?>