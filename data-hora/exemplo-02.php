<?php

$ts = strtotime("2001-09-11"); //strtotime retorna um timestamp
echo $ts . " - Timestamp de 11 de setembro";
echo "<br>";
echo date("l, d/m/Y", $ts); //l é pra saber o dia da semana

echo "<hr>";

$ts2 = strtotime("now");
echo "<br>";
echo $ts2 . " - Timestamp de agora";
echo "<br>";
echo date("l, d/m/Y", $ts2);

echo "<hr>";

$ts3 = strtotime("2019-05-16"); //timestamp de meia-noite
echo "<br>";
echo $ts3 . " - Timestamp de hoje";

echo "<hr>";

$ts4 = strtotime("+1 week 2 days");
echo date("l, d/m/Y", $ts4);

echo "<hr>";

$ts5 = strtotime("1970-01-01 00:00:00");
echo $ts5;

?>