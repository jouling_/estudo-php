<?php

header("Content-Type: image/png");
$image = imagecreate(256, 256);

$black = imagecolorallocate($image, 0, 0, 0); //primeira cor criada é o background
$red = imagecolorallocate($image, 255, 0, 0);

imagestring($image, 5, 100, 120, "Teste", $red); //imagem, tamanho da fonte (1-5), x, y, string, color
imagepng($image);
imagedestroy($image);
?>