<?php

namespace Cliente;

class Cadastro extends \Cadastro{
    public function registrarVenda(){
        echo "Foi registrada uma venda para o cliente" . $this->getNome();
    }
}

$cad = new Cadastro();
$cad->setNome("Juliana");
$cad->setEmail("ju@gmail.com");
$cad->setSenha("123456");

echo $cad;

$cad->registrarVenda();

?>