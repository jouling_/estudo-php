<?php

//funções recursivas: somente em ocasiões excepcionais
//primeiro array: pois cada cargo pode ter vários subordinados
//segundo array: pois cada cargo tem diversas informações

header('Content-Type: text/html; charset=utf-8');

$hierarquia = array(
    array(
        'nome_cargo'=>'CEO',
        'nome'=>'Juliana',
        'subordinados'=>array(
            //Inicio: Diretor Comercial
            array(
                'nome_cargo'=>'Diretor Comercial',
                'nome'=>'Wellington',
                'subordinados'=>array(
                    //Inicio: Gerente de vendas
                    array(
                        'nome_cargo'=>'Gerente de vendas',
                        'nome'=>'Mari'
                    )
                    //Termino: Gerente de vendas
                )
            ),
            //Término: Diretor Comercial

            //Inicio: Diretor Financeiro
            array(
                'nome_cargo'=>'Diretor Financeiro',
                'nome'=>'Camila',
                'subordinados'=>array(
                    //Inicio: Gerente de contas a pagar
                    array(
                        'nome_cargo'=>'Gerente de contas a pagar',
                        'nome'=>'Biabia',
                        'subordinados'=>array(
                            //Inicio: Supervisor de pagamentos 
                            array(
                                'nome_cargo'=>'Supervisor de pagamentos',
                                'nome'=>'Matheus',
                                'subordinados'=>array(
                                    //Início: Funcionário de pagamentos
                                    array(
                                        'nome_cargo'=>'Funcionário de pagamentos',
                                        'nome'=>'Qualquer um'
                                    )
                                    //Término: Funcionário de pagamentos
                                )
                            )
                            //Término: Supervisor de pagamentos 
                        )
                    ),
                    //Termino: Gerente de contas a pagar
                    //Inicio: Gerente de compras
                    array(
                        'nome_cargo'=>'Gerente de compras',
                        'nome'=>'Caca',
                        'subordinados'=>array(
                            //Inicio: Supervisor de suprimentos
                            array(
                                'nome_cargo'=>'Supervisor de suprimentos',
                                'nome'=>'Rafa',
                                'subordinados'=>array(
                                    //Inicio: Funcionário de estoque
                                    array(
                                        'nome_cargo'=>'Funcionário de estoque',
                                        'nome'=>'Samu'
                                    )
                                    //Termino: Funcionário de estoque
                                )
                            )
                            //Termino: Supervisor de suprimentos
                        )
                    )
                    //Termino: Gerente de compras
                )
            )
            //Termino: Diretor Financeiro
        )
    )
 );

 function exibe($cargos){
    $html = '<ul>';

    foreach ($cargos as $value) {
        $html .= "<li>";

        $html .= $value['nome_cargo'] ." - ". $value['nome'];

        if(isset($value['subordinados'])){
            $html .= exibe($value['subordinados']); //há uma lista de cargos dentro dele
        }

        $html .= "</li>";
    }

    $html .= '</ul>';

    return $html;
 }

 echo exibe($hierarquia);

?>