<?php

//passagem de parâmetro por referência

$a = 10;

function trocaValor(&$b){
    $b += 50;
    return $b;
}

echo $a; //10
echo "<br>";
echo trocaValor($a); //60
echo "<br>";
echo $a; //60
echo "<br>";
echo trocaValor($a); //110

?>