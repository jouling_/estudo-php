<?php

//passagem de parâmetro por valor

$a = 10;

function trocaValor($b){
    $b += 50;
    return $b;
}

echo $a; //10

echo "<br>";

echo trocaValor($a); //60

echo "<br>";

echo $a; //10

?>