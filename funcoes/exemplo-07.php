<?php

header('Content-Type: text/html; charset=utf-8');

$pessoa = array(
    'nome'=>'João',
    'idade'=>20
);

foreach ($pessoa as $key => $value) {
    echo $key . "<br>" . $value . "<br>";
}
/*
nome
João
idade
20
*/

echo "<br>";
foreach ($pessoa as &$value) {
    if(gettype($value) === 'integer'){
        $value += 10; 
    }

    echo $value . "<br>"; //joão fica com 30 anos
}

echo "<br>";
print_r($pessoa);

?>