<?php

header('Content-Type: text/html; charset=utf-8');

/*function ola($nome = "mundo", $periodo = "Bom dia"){ //não é mais obrigatório passar parâmetro; por padrão vai o mundo
    return "Olá $nome! $periodo<br>"; //com aspas simples imprimiria "Olá $nome!" 
}

echo ola("Juliana", "Boa tarde"); //Olá Juliana! Boa tarde
echo ola("", "Boa noite"); //imprime "Olá ! Boa noite"
echo ola(); //imprime "Olá mundo! Bom dia"
echo ola("", "") //Olá !
*/

/*function ola2($nome = "Mundo", $periodo){ //não é mais obrigatório passar parâmetro; por padrão vai o mundo
    return "Olá $nome! $periodo!<br>"; //com aspas simples imprimiria "Olá $nome!" 
}

echo ola2("Juliana", "Boa tarde"); //Olá Juliana! Boa tarde!
echo ola2("", "Boa noite"); //imprime "Olá ! Boa noite!"
echo ola2(); //Olá Mundo! !
echo ola2("mundo"); //Olá mundo! !
echo ola2("blabla") //Olá blabla! !
*/

function ola3($nome, $periodo){ //não é mais obrigatório passar parâmetro; por padrão vai o mundo
    return "Olá $nome! $periodo!<br>"; //com aspas simples imprimiria "Olá $nome!" 
    $argumentos = func_get_args(); //função recupera os argumentos e passa num array 
    return $argumentos;
}

echo ola3("Juliana", "Boa tarde"); //Olá Juliana! Boa tarde!
echo ola3("", "Boa noite"); //imprime "Olá ! Boa noite!"
echo ola3(); //Olá ! !
echo ola3("mundo"); //Olá mundo! !
echo ola3("blabla") //Olá blabla! !
?>