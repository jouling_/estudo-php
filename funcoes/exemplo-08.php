<?php

function soma(int ...$valores){
    return array_sum($valores);
}

echo var_dump(soma(10, 10, 10, 10, 10, 10, 10)) . "<br>"; //int(70)
echo var_dump(soma(25, 33)) . "<br>"; //int(58)
echo var_dump(soma(2.5, 3.3)) . "<br>"; //int(5) pois disse que seriam inteiros

echo "<br>";

function soma2(float ...$valores):string{
    return array_sum($valores);
}

echo var_dump(soma2(10, 10, 10, 10, 10, 10, 10)) . "<br>"; //string(2) "70" 
echo var_dump(soma2(25, 33)) . "<br>"; //string(2) "58"
echo var_dump(soma2(2.5, 3.3)) . "<br>";//string(3) "5.8"

echo "<br>";

function soma3(float ...$valores){
    return array_sum($valores);
}

echo var_dump(soma3(10, 10, 10, 10, 10, 10, 10)) . "<br>"; //float(70)
echo var_dump(soma3(25, 33)) . "<br>"; //float(58)
echo var_dump(soma3(2.5, 3.3)) . "<br>"; //float(5.8)


?>