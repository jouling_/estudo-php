<?php

try{
    throw new Exception("Error", 400); //o número é o código do erro
} catch(Exception $e){
    echo json_encode(array(
        "message"=>$e->getMessage(),
        "line"=>$e->getLine(),
        "file"=>$e->getFile(),
        "code"=>$e->getCode()        
    ));
}

?>