<?php

function trataNome($name){
    if(!$name){
        throw new Exception("Nenhum nome foi informado", 1);
    } 
    echo ucfirst($name). "<br>";
}

try{
    trataNome("joao");
    trataNome("");
}catch(Exception $e){
    echo $e->getMessage();
} finally{ //independentemente de executar o try ou o catch
    echo "Executou o Try";
}

?>