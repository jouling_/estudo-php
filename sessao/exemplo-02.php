<?php

require_once("config.php");

//limpa a variável
session_unset($_SESSION["nome"]); //se não passar nenhum parâmetro no session, ele limpa todas as variáveis

echo $_SESSION["nome"] = "Hcode";
//ao executar, ele diz que não há tal index 

session_unset($_SESSION["nome"]);

echo $_SESSION["nome"];

//limpa a variável e remove o usuário
session_destroy();

//tenho que criar $_SESSION["nome"] = "criei-dnv"
?>